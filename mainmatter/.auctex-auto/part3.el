(TeX-add-style-hook
 "part3"
 (lambda ()
   (LaTeX-add-labels
    "chapter:part3"
    "fig:WindTunnelSchematic"
    "fig:SupersonicWindTunnel_1"
    "fig:SupersonicWindTunnel_2"
    "fig:SupersonicWindTunnel_3"
    "fig:SupersonicWindTunnel_4"
    "fig:SupersonicWindTunnel_5"
    "fig:SupersonicWindTunnel_6"
    "eq:area_mach_relation"
    "fig:wind_tunnel_normal_shock"
    "tab:relation_between_1_and_2"
    "tab:relation_between_2_and_U"
    "tab:relation_between_U_and_D"
    "tab:relation_between_D_and_E"
    "fig:wind_tunnel_stag_p_5_bar_mini"
    "fig:wind_tunnel_stag_p_6_bar_mini"
    "fig:wind_tunnel_stag_p_7_bar_mini"
    "fig:wind_tunnel_stag_p_8_bar_mini"
    "fig:fanno_flow_picture_modified"
    "fig:fanno_flow_picture_excel"
    "fig:rayleigh_flow_picture_excel")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0)))
 :latex)

