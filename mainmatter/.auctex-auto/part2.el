(TeX-add-style-hook
 "part2"
 (lambda ()
   (LaTeX-add-labels
    "chapter:part2"
    "fig:apparatus_labeled"
    "fig:nozzle_gauges"
    "fig:compressor_interface"
    "eq:massFlowRate_equals_density_area_velocity"
    "eq:velocity_equals_Mach_speedSound"
    "eq:massFlowRate_equals_density_area_Mach_speedOfSound"
    "eq:stag_temperature_temperature_ratio"
    "eq:Mach_temperature_stagnationTemperature"
    "eq:ideal_gas_law"
    "eq:isentropic_gas_relations"
    "eq:subsonic_compressible_flow_equation"
    "eq:numerical_Rp"
    "eq:choked_mass_flow_rate_equation"
    "fig:mass_flow_rate_series_1"
    "fig:mass_flow_rate_series_2"
    "fig:pressure_distribution_NS34_series1_mini"
    "fig:pressure_distribution_NS34_series2_mini"
    "fig:pressure_distribution_NS45_series1_mini"
    "fig:pressure_distribution_NS45_series2_mini"
    "fig:pressure_distribution_NS56_series1_mini"
    "fig:pressure_distribution_NS56_series2_mini"
    "fig:pressure_distribution_NS67_series1_mini"
    "fig:pressure_distribution_NS67_series2_mini"
    "fig:pressure_distribution_NS78_series1_mini"
    "fig:pressure_distribution_NS78_series2_mini")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0)))
 :latex)

