(TeX-add-style-hook
 "part1"
 (lambda ()
   (LaTeX-add-labels
    "chapter:part1")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0)))
 :latex)

