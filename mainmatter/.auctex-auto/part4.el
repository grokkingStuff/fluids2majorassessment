(TeX-add-style-hook
 "part4"
 (lambda ()
   (LaTeX-add-labels
    "chapter:part4")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0)))
 :latex)

