(TeX-add-style-hook
 "introduction"
 (lambda ()
   (LaTeX-add-labels
    "chapter:introduction"
    "fig:SupersonicWindTunnel_1"
    "fig:SupersonicWindTunnel_2"
    "fig:SupersonicWindTunnel_3"
    "fig:SupersonicWindTunnel_4"
    "fig:SupersonicWindTunnel_5"
    "fig:SupersonicWindTunnel_6"))
 :latex)

