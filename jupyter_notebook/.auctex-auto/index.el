(TeX-add-style-hook
 "index"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "amssymb"
    "capt-of"
    "hyperref"
    "biblatex")
   (LaTeX-add-labels
    "sec:org2195e4a"
    "sec:org94fd650"
    "sec:org0ab6de1"
    "sec:orga2cdf5e"
    "sec:orgdaeb736"
    "sec:orgd73a469"
    "sec:org4cdad25"
    "sec:org2d8b198"
    "sec:org49f863f"
    "sec:org6f96143"
    "sec:org35ff56d"
    "sec:org375fc89"
    "sec:org06c6327"
    "sec:orgd60cc8a"
    "sec:orga0688ea"
    "sec:orgd9fe90a"))
 :latex)

