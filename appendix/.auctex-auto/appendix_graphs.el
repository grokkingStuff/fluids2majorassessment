(TeX-add-style-hook
 "appendix_graphs"
 (lambda ()
   (LaTeX-add-labels
    "appendix:graphs"
    "fig:pressure_distribution_NS34_series1"
    "fig:pressure_distribution_NS34_series2"
    "fig:pressure_distribution_NS45_series1"
    "fig:pressure_distribution_NS45_series2"
    "fig:pressure_distribution_NS56_series1"
    "fig:pressure_distribution_NS56_series2"
    "fig:pressure_distribution_NS67_series1"
    "fig:pressure_distribution_NS67_series2"
    "fig:pressure_distribution_NS78_series1"
    "fig:pressure_distribution_NS78_series2")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0)))
 :latex)

