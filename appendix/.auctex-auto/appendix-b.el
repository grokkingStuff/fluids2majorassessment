(TeX-add-style-hook
 "appendix-b"
 (lambda ()
   (LaTeX-add-labels
    "tab:taskdivision"
    "tab:contribution_table")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0)))
 :latex)

