(TeX-add-style-hook
 "report"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "table") ("colortbl" "table")))
   (TeX-run-style-hooks
    "latex2e"
    "frontmatter/title-report"
    "mainmatter/part1"
    "mainmatter/part2"
    "mainmatter/part3"
    "mainmatter/part4"
    "appendix/appendix_graphs"
    "appendix/appendix-b"
    "layout/tudelft-report"
    "layout/tudelft-report10"
    "xcolor"
    "colortbl"
    "placeins"
    "array"
    "multirow"
    "graphicx"
    "rotating"
    "biblatex")
   (TeX-add-symbols
    "rot")
   (LaTeX-add-environments
    '("variables" LaTeX-env-args ["argument"] 0))
   (LaTeX-add-bibliographies)
   (LaTeX-add-lengths
    "conditionwd"))
 :latex)

